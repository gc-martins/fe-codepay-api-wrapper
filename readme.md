# JS flowtype wrapper for codepay api

# Usage
4 steps to use

0) add this to dependencies

```
"git+ssh://git@bitbucket.org:gamecredz/fe-codepay-api-wrapper.git#<VERSION>",
```

1) add next lines to .flowconfig
```
module.name_mapper='^fe-codepay-api-wrapper$' -> '<PROJECT_ROOT>/node_modules/fe-codepay-api-wrapper/wrapper/async.js'
```
gportal example:
https://bitbucket.org/gamecredz/gportal/src/4fa3277bb7568a93f42bacfb12525d5a27fad013/.flowconfig#lines-41

2) add next lines to to .babelrc
```
["transform-imports", {
  "fe-codepay-api-wrapper": {
    "transform": "fe-codepay-api-wrapper/wrapper/src/async/${member}"
  }
}]
```
additionally use can use  "preventFullImport": true for webapps to minimize bundle side
```
["transform-imports", {
  "fe-codepay-api-wrapper": {
    "transform": "fe-codepay-api-wrapper/wrapper/src/async/${member},
    "preventFullImport": true
  }
}]
```
gportal example: https://bitbucket.org/gamecredz/gportal/src/4fa3277bb7568a93f42bacfb12525d5a27fad013/.babelrc#lines-37

3) in your app runtime before usage of api, you need to set wrapper runtime

f.e. in your app

```
import {httpRequest} from './sagas/http';
import {baseURL} from './services/api';

import {
  setBaseURL,
  setHttpRequest,
} from 'fe-codepay-api-wrapper/src/helpers/runtime';

  setBaseURL(BASE_URL.slice(0, BASE_URL.indexOf('/api/')));
  setHttpRequest(({ method, url, data, options }) => {
    return Http.request(method, url, data, options);
  });
```

- setBaseURL - setting base url for all requests

  BASE_URL here is https://api-stage.gamecredits.com/api/v1 f.e.
  because wrapper already binded to some api version we need to remove `v1` here  `BASE_URL.slice(0, BASE_URL.indexOf('/api/'))`

- setHttpRequest - this is hook where you receive data of requests, and send actuall request with some lower level tool: axious, fetch, your custom saga (https://bitbucket.org/gamecredz/web-wallet-rjs/src/master/src/sagas/http.js) or custom http service (https://bitbucket.org/gamecredz/gportal/src/4fa3277bb7568a93f42bacfb12525d5a27fad013/app/services/http/)

`

you can also use replace `fe-codepay-api-wrapper/wrapper/src/async/` with `fe-codepay-api-wrapper/wrapper/src/gen/` if you want to use generators instead of async/promises

# Regenerate wrapper
```
npm run create-wrapper
```
# Publish

bump package.json tag and push same version tag to Git

# jenkins and CI

jenkins task https://jenkins.gamecredits.org/job/Codepay/job/fe-codepay-api-wrapper-merge-tag/ that recreates wrapper

you can provide swaggerYaml in params, of in none is provided task get lastest codepay swagger schema codepay stage
```
npm install json json2yaml -g
curl https://api-stage.gamecredits.com/api/v1/api-docs?group=api | json | json2yml > swagger.yaml
```

then
1) some patches applied on schema `npm run patch-schema`
2) wrapped gets regenerated
3) if git index is not empty and flow checks passing
4) new version is released

white task is running it notifies about it's stages in rocket chat channel: #fe-codepay-api-wrapper-ci
