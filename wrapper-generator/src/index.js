// @flow

import parseArgs from 'command-line-args';
import browserify from 'browserify';
import printUsage from 'command-line-usage';
import fs from 'fs';
import path from 'path';
import _ from 'lodash';
import yaml from 'js-yaml';

import resolvePath from './resolvePath';
import convertSwaggerToFiles from './convertSwaggerToFiles';
import packageJson from '../package.json';

const optionDefs = [
  {
    name: 'inputYml',
    alias: 'y',
    description: 'Path to Swagger YML file to convert',
    type: String,
  },
  {
    name: 'input',
    alias: 'i',
    description: 'Path to Swagger JSON file to convert',
    type: String,
  },
  {
    name: 'output',
    alias: 'o',
    description: 'Folder path To Output generator package to',
    type: String,
  },
  {
    name: 'name',
    alias: 'n',
    description: 'Name for the generated package',
    type: String,
  },
  {
    name: 'version',
    alias: 'v',
    description: 'Version number for the generator NPM/Bower modules',
    type: String,
  },
  {
    name: 'help',
    alias: 'h',
    description: 'Prints this usage guide',
    type: Boolean,
    defaultOption: false,
  },
];

const usageGuide = [
  {
    header: 'Swagger to Javascript API generator',
    content:
      'Takes a Swagger JSON file and generates UMD NPM and bower package to be used in Javascript applications.',
  },
  {
    header: 'Options',
    optionList: optionDefs,
  },
];

const options = parseArgs(optionDefs);

options.version =
  options.version ||
  `1.0.${process.env.BUILD_NUMBER || Math.floor(Math.random() * 1000)}`;

if (options.help) {
  console.log(`swagger-to-js-api — v${packageJson.version}`);
  console.log(printUsage(usageGuide));
  process.exit(1);
}

// if (!options.input) {
//   console.error(
//     'Need path to JSON file as input. Please use the `-i` flag to pass it in.'
//   )
//   process.exit(1)
// }

if (!options.output) {
  console.error(
    'Need path to destination folder. Please use the `-o` flag to pass it in.',
  );
  process.exit(1);
}

if (!options.name) {
  console.error(
    'Need a name for the generated pacakge. Please use the `-n` flag to pass it in.',
  );
  process.exit(1);
}

let input = options.inputYml ? options.inputYml : options.input;

input = resolvePath(input);
options.output = resolvePath(options.output);

let jsonFile;
if (!options.inputYml) {
  jsonFile = JSON.parse(fs.readFileSync(input, 'utf-8'));
} else {
  jsonFile = yaml.safeLoad(fs.readFileSync(options.inputYml, 'utf8'));
}

convertSwaggerToFiles(jsonFile, options);

browserify({ standalone: _.camelCase(options.name) })
  .add(path.join(options.output, './index.js'))
  .bundle()
  .pipe(fs.createWriteStream(path.join(options.output, './dist/index.js')));
