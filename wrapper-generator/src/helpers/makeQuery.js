// @flow

/* eslint-disable spaced-comment */

export default (obj: Object): string => {
  if (!obj) {
    return '';
  }

  const keys = Object.keys(obj);
  if (!keys.length) {
    return '';
  }

  return `?${keys
    .map(key => {
      let value = obj[key];
      if (typeof value === 'object') {
        value = JSON.stringify(value);
      }
      return `${encodeURIComponent(key)}=${encodeURIComponent(value)}`;
    })
    .join('&')}`;
};
