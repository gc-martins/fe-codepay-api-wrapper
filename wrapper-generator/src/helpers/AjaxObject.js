// @flow

export type AjaxObject = {
  method: 'GET' | 'POST' | 'PUT' | 'DELETE' | 'PATCH' | 'OPTIONS',
  url: string,
  data?: any,
};

export type AjaxResponse<T> = {
  config: Object,
  data: T,
  headers: Object,
  request: Object,
  status: number,
  statusText: string,
};
