// @flow

/* global File, FormData */
/* eslint-disable spaced-comment */

function isObject(value: any): boolean {
  return value === Object(value);
}

function isArray(value: any): boolean {
  return Array.isArray(value);
}

function isFile(value: any): boolean {
  return value instanceof File;
}

export default function objectToFormData(
  obj: Object | Array<any>,
  formData: ?FormData,
  preKey: ?string,
): FormData {
  formData = formData || new FormData();

  const objOrArray = obj;

  const array = Array.isArray(objOrArray)
    ? objOrArray
    : Object.keys(objOrArray);

  array.forEach(prop => {
    const key = preKey ? `${preKey}[${prop}]` : prop;

    if (isObject(obj[prop]) && !isArray(obj[prop]) && !isFile(obj[prop])) {
      objectToFormData(obj[prop], formData, key);
    } else if (isArray(obj[prop])) {
      obj[prop].forEach(value => {
        const arrayKey = `${key}[]`;

        if (isObject(value) && !isFile(value)) {
          objectToFormData(value, formData, arrayKey);
        } else if (formData) {
          formData.append(arrayKey, value);
        }
      });
    } else if (formData) {
      formData.append(key, obj[prop]);
    }
  });

  return formData;
}
