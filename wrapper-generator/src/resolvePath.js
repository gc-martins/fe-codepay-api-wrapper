// @flow

import path from 'path';

export default (folderPath: string) => {
  if (folderPath[0] === '/' || folderPath[0] === '~') {
    return folderPath;
  }
  return path.join(process.cwd(), folderPath);
};
