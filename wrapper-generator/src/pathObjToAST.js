// @flow

import BabelTypes from 'flow-babel-types';
import get from 'lodash/get';
import isEqual from 'lodash/isEqual';
import uniq from 'lodash/uniq';
import swaggerTypeToFlowType from './swaggerTypeToFlowType';

const t = new BabelTypes(require('babel-types'));

type UrlPartsT = {
  quasis: Array<any>,
  expressions: Array<any>,
};

type pathObjT = {
  apiType: 'async' | 'gen',
  parameters: Array<*>,
  path: string,
  method: string,
  operationId: string,
};

type keyParamsT = {
  apiType: 'async' | 'gen',
  responseTypeName?: string,
  pathObjParameters: Array<*>,
  bodyParamJson: {
    schema: Object,
    type: string,
  },
  urlParts: UrlPartsT,
  operationId: string,
  path: string,
  method: string,
  hasQuery: boolean,
  hasBody: boolean,
  hasFormData: boolean,
};

const getUrlParamsAndParts = (
  str: string,
): { urlParts: UrlPartsT, urlParams: Array<string> } => {
  let urlParams = [];
  // $FlowIssue
  const urlParts: UrlPartsT = str.split(/(\}|\{)/).reduce((
    compiled: { mode: string, quasis: Array<*>, expressions: Array<*> },
    current: string,
  ) => {
    if (current === '{') {
      return { ...compiled, mode: 'variable' };
    }
    if (current === '}') {
      return { ...compiled, mode: 'string' };
    }
    if (compiled.mode === 'string') {
      compiled.quasis = [
        ...compiled.quasis,
        t.templateElement({ raw: current, cooked: current }),
      ];
      return compiled;
    }
    if (compiled.mode === 'variable') {
      urlParams = [...urlParams, current];
      compiled.expressions = [...compiled.expressions, t.identifier(current)];
      return compiled;
    }
  }, { quasis: [], expressions: [], mode: 'string' });
  return { urlParams: urlParams.sort(), urlParts };
};

const getTypeImports = typeImports => {
  // Create a AST object for `Program` that includes the imports and function
  // and returns it along with the name of the function so it can be written to
  // a file.
  return uniq(typeImports).map(name => {
    const importStatement = t.importDeclaration(
      [t.importSpecifier(t.identifier(name), t.identifier(name))],
      t.stringLiteral(`../types/${name}`),
    );
    // $FlowIssue
    importStatement.importKind = 'type';

    return importStatement;
  });
};

const getAjaxObjectImport = () => {
  const ajaxObjectImport = t.importDeclaration(
    [
      t.importSpecifier(t.identifier('AjaxObject'), t.identifier('AjaxObject')),
      t.importSpecifier(
        t.identifier('AjaxResponse'),
        t.identifier('AjaxResponse'),
      ),
    ],
    t.stringLiteral('../types/AjaxObject'),
  );
  // $FlowIssue
  ajaxObjectImport.importKind = 'type';

  return ajaxObjectImport;
};

const getImports = ({
  hasQuery,
  hasFormData,
}: {
  hasQuery: boolean,
  hasFormData: boolean,
}) => {
  return [
    t.importDeclaration(
      [
        t.importSpecifier(
          t.identifier('getHttpRequest'),
          t.identifier('getHttpRequest'),
        ),
        t.importSpecifier(
          t.identifier('getBaseURL'),
          t.identifier('getBaseURL'),
        ),
      ],
      t.stringLiteral('../helpers/runtime'),
    ),
    // declare imports for the helpers that are used in the function.
    ...(hasQuery
      ? [
          t.importDeclaration(
            [t.importDefaultSpecifier(t.identifier('makeQuery'))],
            t.stringLiteral('../helpers/makeQuery'),
          ),
        ]
      : []),
    ...(hasFormData
      ? [
          t.importDeclaration(
            [t.importDefaultSpecifier(t.identifier('makeFormData'))],
            t.stringLiteral('../helpers/makeFormData'),
          ),
        ]
      : []),
    getAjaxObjectImport(),
  ];
};

const getGeneratorFunctionNode = () => {
  const generatorFunctionNode = t.functionDeclaration(
    t.identifier('Gen'),
    [getObjParams()],
    t.blockStatement([
      t.returnStatement(
        t.yieldExpression(
          t.callExpression(
            t.callExpression(t.identifier('getHttpRequest'), []),
            [
              t.callExpression(t.identifier('getParams'), [
                t.identifier('obj'),
              ]),
            ],
          ),
        ),
      ),
    ]),
    true,
  );

  generatorFunctionNode.returnType = t.typeAnnotation(
    t.genericTypeAnnotation(
      t.identifier('Generator'),
      t.typeParameterInstantiation([
        t.genericTypeAnnotation(t.identifier('*'), null),
        t.genericTypeAnnotation(t.identifier('Response'), null),
        t.genericTypeAnnotation(t.identifier('*'), null),
      ]),
    ),
  );
  return generatorFunctionNode;
};

const getAsyncFunctionNode = () => {
  const arrowFunction = t.arrowFunctionExpression(
    [getObjParams()],
    t.blockStatement([
      t.returnStatement(
        t.awaitExpression(
          t.callExpression(
            t.callExpression(t.identifier('getHttpRequest'), []),
            [
              t.callExpression(t.identifier('getParams'), [
                t.identifier('obj'),
              ]),
            ],
          ),
        ),
      ),
    ]),
    true,
  );

  const result = t.variableDeclaration('const', [
    t.variableDeclarator(t.identifier('Async'), arrowFunction),
  ]);

  // $FlowIssue
  arrowFunction.returnType = t.typeAnnotation(
    t.genericTypeAnnotation(
      t.identifier('Promise'),
      t.typeParameterInstantiation([
        t.genericTypeAnnotation(t.identifier('Response'), null),
      ]),
    ),
  );
  return result;
};

const getDefaultExport = extractIdentifierSting => {
  return t.exportDefaultDeclaration(t.identifier(extractIdentifierSting));
};

const getGetParamsFunc = ({
  hasBody,
  method,
  hasFormData,
  urlParts,
  hasQuery,
  destructuredParams,
}: {
  hasBody: boolean,
  method: string,
  hasFormData: boolean,
  hasQuery: boolean,
  urlParts: UrlPartsT,
  destructuredParams: *,
}) => {
  return t.variableDeclaration('const', [
    t.variableDeclarator(
      t.identifier('getParams'),
      t.functionExpression(
        null,
        [getObjParams()],
        // Create the properties that will put on the returned object
        t.blockStatement([
          t.variableDeclaration('const', [
            t.variableDeclarator(
              t.objectPattern(
                [
                  ...(destructuredParams.length
                    ? [
                        t.objectProperty(
                          t.identifier('parameters'),
                          t.objectPattern(
                            destructuredParams.map(identifier => {
                              return t.objectProperty(
                                identifier,
                                identifier,
                                false,
                                true,
                                null,
                              );
                            }),
                          ),
                          false,
                          true,
                          null,
                        ),
                      ]
                    : []),
                  t.objectProperty(
                    t.identifier('options'),
                    t.identifier('options'),
                    false,
                    true,
                    null,
                  ),
                ],
                null,
              ),
              t.identifier('obj'),
            ),
          ]),
          t.returnStatement(
            t.objectExpression([
              t.objectProperty(
                t.identifier('method'),
                t.stringLiteral(method.toUpperCase()),
              ),
              t.objectProperty(
                t.identifier('url'),
                t.binaryExpression(
                  '+',
                  t.callExpression(t.identifier('getBaseURL'), []),
                  // If the endpoint accepts query params, + a queryString at the end of the pathname
                  hasQuery
                    ? t.binaryExpression(
                        '+',
                        t.templateLiteral(
                          urlParts.quasis,
                          urlParts.expressions,
                        ),
                        t.callExpression(t.identifier('makeQuery'), [
                          t.identifier('query'),
                        ]),
                      )
                    : t.templateLiteral(urlParts.quasis, urlParts.expressions),
                ),
              ),
              // if the endpoint takes a post-body, add that as a key to the object
              ...(hasBody
                ? [
                    t.objectProperty(
                      t.identifier('data'),
                      hasFormData
                        ? t.callExpression(t.identifier('makeFormData'), [
                            t.identifier('data'),
                          ])
                        : t.identifier('data'),
                      false,
                      true,
                      null,
                    ),
                  ]
                : []),
              t.objectProperty(
                t.identifier('options'),
                t.identifier('options'),
                false,
                true,
                null,
              ),
            ]),
          ),
        ]),
      ),
    ),
  ]);
};

const getObjParams = () => {
  const obj = t.identifier('obj');
  obj.typeAnnotation = t.typeAnnotation(
    t.genericTypeAnnotation(t.identifier('ObjT'), null),
  );
  return obj;
};

const getQueryParamsDeclaration = ({
  hasQuery,
  pathObjParameters,
  typeImports,
}) => {
  const queryParam = t.identifier('query');

  const queryParamType = swaggerTypeToFlowType(
    {
      type: 'object',
      properties: pathObjParameters
        .filter(({ in: paramsIn }) => paramsIn === 'query')
        .map(param => ({ [param.name]: param }))
        .reduce((obj, current) => ({ ...obj, ...current }), {}),
    },
    typeImports,
  );

  return {
    paramsType: [
      ...(hasQuery ? [t.objectTypeProperty(queryParam, queryParamType)] : []),
    ],
    destructuredParams: [...(hasQuery ? [queryParam] : [])],
  };
};

const getBodyParamsDeclarationType = ({
  hasBody,
  hasFormData,
  pathObjParameters,
  typeImports,
  bodyParamJson,
}) => {
  if (hasBody) {
    if (hasFormData) {
      return swaggerTypeToFlowType(
        {
          type: 'object',
          properties: pathObjParameters
            .filter(({ in: paramsIn }) => paramsIn === 'formData')
            .map(param => ({ [param.name]: param }))
            .reduce((obj, current) => ({ ...obj, ...current }), {}),
        },
        typeImports,
      );
    } else if (bodyParamJson.schema) {
      return swaggerTypeToFlowType(bodyParamJson.schema, typeImports);
    } else if (bodyParamJson.type) {
      return swaggerTypeToFlowType(bodyParamJson, typeImports);
    }
  }
  return null;
};

const getBodyParamsDeclaration = ({ hasBody }) => {
  const bodyParam = t.identifier('data');

  return {
    paramsType: [
      ...(hasBody
        ? [t.objectTypeProperty(bodyParam, t.identifier('ParamsDataT'))]
        : []),
    ],
    destructuredParams: [...(hasBody ? [bodyParam] : [])],
  };
};

const getParamsParamsDeclaration = ({ pathObjParameters }) => {
  return {
    paramsType: [
      ...pathObjParameters
        .filter(({ in: paramsIn, required }) => paramsIn === 'path' && required)
        .map(param => {
          const { name, schema, type } = param;
          const paramName = t.identifier(name);

          let typeAnnotation = t.identifier('any');
          if (schema) {
            typeAnnotation = swaggerTypeToFlowType(schema);
          } else if (type) {
            typeAnnotation = swaggerTypeToFlowType(param);
          }

          return t.objectTypeProperty(paramName, typeAnnotation);
        }),
    ],

    destructuredParams: [
      ...pathObjParameters
        .filter(({ in: paramsIn, required }) => paramsIn === 'path' && required)
        .map(({ name }) => t.identifier(name)),
    ],
  };
};

const getParamsDeclaration = ({
  hasQuery,
  hasBody,
  hasFormData,
  pathObjParameters,
  typeImports,
  bodyParamJson,
}) => {
  const queryParamsDeclaration = getQueryParamsDeclaration({
    hasQuery,
    pathObjParameters,
    typeImports,
  });

  const bodyParamsDeclaration = getBodyParamsDeclaration({
    hasBody,
    hasFormData,
    pathObjParameters,
    typeImports,
    bodyParamJson,
  });

  const paramsParamsDeclaration = getParamsParamsDeclaration({
    pathObjParameters,
    typeImports,
  });

  return {
    paramsType: [
      ...paramsParamsDeclaration.paramsType,
      ...queryParamsDeclaration.paramsType,
      ...bodyParamsDeclaration.paramsType,
    ],

    destructuredParams: [
      ...paramsParamsDeclaration.destructuredParams,
      ...queryParamsDeclaration.destructuredParams,
      ...bodyParamsDeclaration.destructuredParams,
    ],
  };
};

const getResponseType = ({ responseTypeName }) => {
  return t.exportNamedDeclaration(
    t.typeAlias(
      t.identifier('Response'),
      null,
      t.genericTypeAnnotation(
        t.identifier('AjaxResponse'),
        t.typeParameterInstantiation([
          responseTypeName
            ? t.genericTypeAnnotation(t.identifier(responseTypeName))
            : t.anyTypeAnnotation(),
        ]),
      ),
    ),
    [],
  );
};

const getParamsType = (paramsType: *) => {
  const objType = t.objectTypeAnnotation(paramsType, [], []);
  // $FlowIssue
  objType.exact = true;
  return [
    ...(paramsType.length
      ? [
          t.exportNamedDeclaration(
            t.typeAlias(t.identifier('ParamsT'), null, objType),
            [],
          ),
        ]
      : []),
  ];
};

const getObjType = (paramsType: *) => {
  const options = t.objectTypeProperty(
    t.identifier('options'),
    t.identifier('Object'),
  );
  const objType = t.objectTypeAnnotation([
    ...(paramsType.length
      ? [
          t.objectTypeProperty(
            t.identifier('parameters'),
            t.identifier('ParamsT'),
          ),
        ]
      : []),
    options,
  ]);
  // $FlowIssue
  objType.exact = true;
  // $FlowIssue
  options.optional = true;
  return t.exportNamedDeclaration(
    t.typeAlias(t.identifier('ObjT'), null, objType),
    [],
  );
};

const extractKeyParams = (pathObj: pathObjT): keyParamsT => {
  const { operationId, path, method } = pathObj;

  const pathObjParameters = pathObj.parameters || [];

  const hasQuery = !!(pathObjParameters || [])
    .filter(({ in: paramsIn }) => paramsIn === 'query').length;

  const [bodyParamJson] = (pathObjParameters || [])
    .filter(({ in: paramsIn, name }) => paramsIn === 'body' && name === 'body');

  const hasFormData = !!(pathObjParameters || [])
    .filter(({ in: paramsIn }) => paramsIn === 'formData').length;

  const hasBody = !!(pathObjParameters || [])
    .filter(
      ({ in: paramsIn }) => paramsIn === 'formData' || paramsIn === 'body',
    ).length;

  let responseTypeName;

  if (
    get(pathObj, 'responses.200.schema') &&
    get(pathObj, 'responses.200.schema').$ref
  ) {
    responseTypeName = get(pathObj, 'responses.200.schema').$ref.replace(
      '#/definitions/',
      '',
    );
  }

  // prepare a template string for the URL that may contain 0 or more url params
  const { urlParams: paramsUsed, urlParts } = getUrlParamsAndParts(path);

  const paramsProvided = pathObjParameters
    .filter(({ in: paramsIn, required }) => paramsIn === 'path' && required)
    .map(({ name }) => name)
    .sort();

  if (!isEqual(paramsUsed, paramsProvided)) {
    throw new Error(`
      There is a problem in the operation ${operationId}.

      The URL of the operation is: ${path} which has the following params:
      ${JSON.stringify(paramsUsed)}

      But the params actually specified are:
      ${JSON.stringify(paramsProvided)}
    `);
  }
  return {
    apiType: pathObj.apiType,
    responseTypeName,
    hasQuery,
    hasBody,
    hasFormData,
    pathObjParameters,
    bodyParamJson,
    urlParts,
    operationId,
    path,
    method,
  };
};

const getProgramNode = ({
  responseTypeName,
  hasQuery,
  hasBody,
  hasFormData,
  pathObjParameters,
  bodyParamJson,
  urlParts,
  operationId,

  method,
  apiType,
}: keyParamsT) => {
  const typeImports = [...(responseTypeName ? [responseTypeName] : [])];
  const { destructuredParams, paramsType } = getParamsDeclaration({
    hasQuery,
    hasBody,
    hasFormData,
    pathObjParameters,
    typeImports,
    bodyParamJson,
  });

  const bodyParamsDeclarationType = getBodyParamsDeclarationType({
    hasBody,
    hasFormData,
    pathObjParameters,
    typeImports,
    bodyParamJson,
  });
  return [
    operationId,
    t.program([
      ...getImports({ hasQuery, hasFormData }),
      ...getTypeImports(typeImports),
      getResponseType({ responseTypeName }),
      ...(bodyParamsDeclarationType
        ? [
            t.exportNamedDeclaration(
              t.typeAlias(
                t.identifier('ParamsDataT'),
                null,
                bodyParamsDeclarationType,
              ),
              [],
            ),
          ]
        : []),
      ...getParamsType(paramsType),
      getObjType(paramsType),
      getGetParamsFunc({
        destructuredParams,
        operationId,
        hasBody,
        method,
        hasFormData,
        urlParts,
        hasQuery,
      }),
      ...(apiType === 'async' ? [getAsyncFunctionNode()] : []),
      ...(apiType === 'gen' ? [getGeneratorFunctionNode()] : []),
      getDefaultExport(apiType === 'gen' ? 'Gen' : 'Async'),
    ]),
  ];
};

export default (pathObj: pathObjT) => getProgramNode(extractKeyParams(pathObj));
