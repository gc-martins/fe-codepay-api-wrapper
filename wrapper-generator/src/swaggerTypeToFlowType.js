// @flow

import includes from 'lodash/includes';
import BabelTypes from 'flow-babel-types';

const t = new BabelTypes(require('babel-types'));

type sTypeTArray = {
  type: 'array',
  items: sTypeT,
};

type sTypeT =
  | {
      type: 'object',
      properties?: Object,
      required?: boolean,
    }
  | {
      type: 'string',
    }
  | {
      type: 'integer' | 'float' | 'int64',
    }
  | {
      type: 'boolean',
    }
  | sTypeTArray
  | {
      type: void,
      $ref: string,
    };

const objectTypeToFlow = (objectType, imports) => {
  const objectTypeProperties = objectType.properties;
  if (!objectTypeProperties) {
    return t.genericTypeAnnotation(t.identifier('Object'), null);
  }

  const properties = Object.keys(objectTypeProperties).map(propName => ({
    ...objectTypeProperties[propName],
    name: propName,
  }));

  const required = objectType.required || [];

  const retVal = t.objectTypeAnnotation(
    properties.map(prop => {
      const propertyDef = t.objectTypeProperty(
        t.identifier(prop.name),
        swaggerTypeToFlowType(prop, imports),
      );
      if (!includes(required, prop.name)) {
        // $FlowIssue
        propertyDef.optional = true;
      }
      return propertyDef;
    }),
  );

  // $FlowIssue
  retVal.exact = true;

  return retVal;
};

const arrayTypeToFlow = (arrayType: sTypeTArray, imports) => {
  return t.genericTypeAnnotation(
    t.identifier('Array'),
    arrayType.items
      ? t.typeParameterInstantiation([
          swaggerTypeToFlowType(arrayType.items, imports),
        ])
      : t.typeParameterInstantiation([t.anyTypeAnnotation()]),
  );
};

export default function swaggerTypeToFlowType(
  sType: sTypeT,
  imports?: Array<*>,
) {
  imports = imports || [];
  if (!sType.type) {
    const sTypeRef = sType.$ref;

    if (sTypeRef && sTypeRef.match(/^#\/definitions/)) {
      imports.push(sTypeRef.replace('#/definitions/', ''));
      return t.genericTypeAnnotation(
        t.identifier(sTypeRef.replace('#/definitions/', '')),
        null,
      );
    }
  } else {
    if (sType.type === 'object') {
      return objectTypeToFlow(sType, imports);
    } else if (sType.type === 'array') {
      return arrayTypeToFlow(sType, imports);
    } else if (sType.type === 'string') {
      return t.stringTypeAnnotation();
    } else if (
      sType.type === 'integer' ||
      sType.type === 'float' ||
      sType.type === 'int64'
    ) {
      return t.numberTypeAnnotation();
    } else if (sType.type === 'boolean') {
      return t.booleanTypeAnnotation();
    }
  }

  return t.anyTypeAnnotation();
}
