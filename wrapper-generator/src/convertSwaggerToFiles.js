// @flow

import fs from 'fs';
import path from 'path';
import BabelTypes from 'flow-babel-types';
import generate from 'babel-generator';
import _ from 'lodash';
import chalk from 'chalk';

import pathObjToAST from './pathObjToAST';

import swaggerTypeToFlowType from './swaggerTypeToFlowType';

const t = new BabelTypes(require('babel-types'));

const flowFilePrefix = `// @flow\n\n`;

const generateTypes = (swaggerObj, writeFileOutput) => {
  const toFindDuplicates = {};
  Object.keys(swaggerObj.definitions)
    .map(defName => {
      if (toFindDuplicates[defName.toLowerCase()]) {
        console.error(`
  ${chalk.red('ERROR:')}
  There are two different types with the name ${defName}, that only differ in case.
  This will cause the files to overwrite each other on case-insensitve file systems
  like the one on macOS.
  `);
      }
      toFindDuplicates[defName.toLowerCase()] = true;
      return { ...swaggerObj.definitions[defName], name: defName };
    })
    .map(typeDef => {
      const { name } = typeDef;
      const imports = [];

      return [name, swaggerTypeToFlowType(typeDef, imports), imports];
    })
    .map(([name, typeAst, nonUniqImports]) => {
      const imports = _.uniq(nonUniqImports);
      const mainExport = t.exportNamedDeclaration(
        t.typeAlias(t.identifier(name), null, typeAst),
        [],
      );
      const program = t.program([
        ...imports.map(importName => {
          const importStatement = t.importDeclaration(
            [
              t.importSpecifier(
                t.identifier(importName),
                t.identifier(importName),
              ),
            ],
            t.stringLiteral(`./${importName}`),
          );
          // $FlowIssue
          importStatement.importKind = 'type';

          return importStatement;
        }),
        mainExport,
      ]);
      return [name, program];
    })
    .map(([name, ast]) => [name, generate(ast, { quotes: 'single' }).code])
    .forEach(([name, code]) => {
      writeFileOutput(
        path.join('src/types/', `${name}.js`),
        `${flowFilePrefix}${code}`,
      );
    });
};

const generateApi = ({
  apiType,
  operations,
  mkDirOutput,
  writeFileOutput,
}: {
  apiType: 'async' | 'gen',
  operations: *,
  mkDirOutput: *,
  writeFileOutput: *,
}) => {
  mkDirOutput(`src/${apiType}`);

  const paths = operations
    .map(obj => pathObjToAST({ ...obj, apiType }))
    .map(([name, ast]) => [name, generate(ast, { quotes: 'single' }).code]);

  const pathNames = _.uniq(paths.map(([name]) => name));

  paths.forEach(([name, code]) => {
    writeFileOutput(
      path.join(`src/${apiType}`, `${name}.js`),
      `${flowFilePrefix}${code}\n`,
    );
  });

  writeFileOutput(
    path.join(`src/${apiType}/Types.js`),
    `
      // @flow
      import * as Types from '../../${apiType}All';
      export default Types;
    `,
  );

  const indexFile = `${flowFilePrefix}${[...pathNames, 'Types']
    .map(
      name =>
        `export { default as ${name}  } from './src/${apiType}/${name}';
    `,
    )
    .join('')}\n
    `;

  writeFileOutput(path.join(`${apiType}.js`), indexFile);

  const indexAllFile = `${flowFilePrefix}${pathNames
    .map(
      name =>
        `export * as ${name} from './src/${apiType}/${name}';
    `,
    )
    .join('')}\n`;

  writeFileOutput(path.join(`${apiType}All.js`), indexAllFile);
};

export default (
  swaggerObj: { basePath: string, paths: Object, definitions: Object },
  options: { output: string },
) => {
  const mkDirOutput = (dest: string) => {
    fs.mkdirSync(path.join(options.output, dest));
  };

  const readFileDirname = (source: string) =>
    fs.readFileSync(path.join(__dirname, source), 'utf-8');

  const writeFileOutput = (dest: string, target: string) => {
    fs.writeFileSync(path.join(options.output, dest), target);
  };

  const copyDirnameOutput = (source: string, dest: string) => {
    writeFileOutput(dest, readFileDirname(source));
  };

  const basePath = swaggerObj.basePath.replace(/\/$/, '');
  const operations = Object.keys(swaggerObj.paths)
    .map(curPath =>
      // flatten the path objects into an array of pathObjects
      Object.keys(swaggerObj.paths[curPath]).map(method => {
        const config = swaggerObj.paths[curPath][method];
        config.method = method;
        config.path = basePath + curPath;
        return config;
      }),
    )
    .reduce((soFar, current) => soFar.concat(current), []);

  const operationIds = _.groupBy(operations, 'operationId');
  const duplicatedOps = Object.keys(operationIds).filter(
    key => operationIds[key].length > 1,
  );

  if (duplicatedOps.length) {
    throw new Error(`
${chalk.red(`The Swagger JSON contains duplicate operationIds for different endpoints.
The following are duplicated:`)}
${JSON.stringify(duplicatedOps, null, 2)}
    `);
  }

  operations.forEach(pathObj => {
    if (!pathObj.summary && !pathObj.description) {
      console.warn(
        `${chalk.yellow(
          'WARNING:',
        )} Summary and discription missing for ${chalk.bold(
          pathObj.operationId,
        )}`,
      );
    }
  });

  mkDirOutput('src');

  mkDirOutput('src/helpers/');
  copyDirnameOutput(
    path.join('./helpers/', 'makeQuery.js'),
    path.join('src/helpers/', 'makeQuery.js'),
  );

  copyDirnameOutput(
    path.join('./helpers/', 'runtime.js'),
    path.join('src/helpers/', 'runtime.js'),
  );

  copyDirnameOutput(
    path.join('./helpers/', 'makeFormData.js'),
    path.join('src/helpers/', 'makeFormData.js'),
  );

  mkDirOutput('src/types/');
  copyDirnameOutput(
    path.join('./helpers/', 'AjaxObject.js'),
    path.join('src/types/', 'AjaxObject.js'),
  );

  generateTypes(swaggerObj, writeFileOutput);

  generateApi({
    apiType: 'async',
    operations,
    mkDirOutput,
    writeFileOutput,
  });

  generateApi({
    apiType: 'gen',
    operations,
    mkDirOutput,
    writeFileOutput,
  });
};
