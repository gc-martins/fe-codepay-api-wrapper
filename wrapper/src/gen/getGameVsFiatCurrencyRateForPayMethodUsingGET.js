// @flow

import { getHttpRequest, getBaseURL } from '../helpers/runtime';
import type { AjaxObject, AjaxResponse } from '../types/AjaxObject';
import type { GetFiatCurrencyVsGameRateOut } from '../types/GetFiatCurrencyVsGameRateOut';
export type Response = AjaxResponse<GetFiatCurrencyVsGameRateOut>;
export type ParamsT = {|
  fiat_currency: string,
  pay_method: string,
|};
export type ObjT = {|
  parameters: ParamsT,
  options?: Object,
|};

const getParams = function(obj: ObjT) {
  const { parameters: { fiat_currency, pay_method }, options } = obj;
  return {
    method: 'GET',
    url:
      getBaseURL() +
      `/api/v1/rate/game/${fiat_currency}/pay-method/${pay_method}`,
    options,
  };
};

function* Gen(obj: ObjT): Generator<*, Response, *> {
  return yield getHttpRequest()(getParams(obj));
}

export default Gen;
