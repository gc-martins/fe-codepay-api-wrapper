// @flow

import { getHttpRequest, getBaseURL } from '../helpers/runtime';
import type { AjaxObject, AjaxResponse } from '../types/AjaxObject';
import type { PendingTransactionResponse } from '../types/PendingTransactionResponse';
export type Response = AjaxResponse<PendingTransactionResponse>;
export type ParamsT = {|
  tx_id: string,
|};
export type ObjT = {|
  parameters: ParamsT,
  options?: Object,
|};

const getParams = function(obj: ObjT) {
  const { parameters: { tx_id }, options } = obj;
  return {
    method: 'GET',
    url: getBaseURL() + `/api/v1/payment/pending-transaction/${tx_id}`,
    options,
  };
};

function* Gen(obj: ObjT): Generator<*, Response, *> {
  return yield getHttpRequest()(getParams(obj));
}

export default Gen;
