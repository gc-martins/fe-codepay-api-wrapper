// @flow

import { getHttpRequest, getBaseURL } from '../helpers/runtime';
import makeFormData from '../helpers/makeFormData';
import type { AjaxObject, AjaxResponse } from '../types/AjaxObject';
import type { RefreshTokenOutput } from '../types/RefreshTokenOutput';
export type Response = AjaxResponse<RefreshTokenOutput>;
export type ParamsDataT = {|
  grant_type?: string,
  client_id?: string,
  client_secret?: string,
  refresh_token?: string,
|};
export type ParamsT = {|
  data: ParamsDataT,
|};
export type ObjT = {|
  parameters: ParamsT,
  options?: Object,
|};

const getParams = function(obj: ObjT) {
  const { parameters: { data }, options } = obj;
  return {
    method: 'POST',
    url: getBaseURL() + `/api/v1/token`,
    data: makeFormData(data),
    options,
  };
};

function* Gen(obj: ObjT): Generator<*, Response, *> {
  return yield getHttpRequest()(getParams(obj));
}

export default Gen;
