// @flow

import { getHttpRequest, getBaseURL } from '../helpers/runtime';
import makeFormData from '../helpers/makeFormData';
import type { AjaxObject, AjaxResponse } from '../types/AjaxObject';
export type Response = AjaxResponse<any>;
export type ParamsDataT = {|
  store_id?: string,
  transaction_id?: string,
  order_id?: string,
  amount?: string,
  client_id?: string,
  client_email?: string,
  currency_code?: string,
|};
export type ParamsT = {|
  data: ParamsDataT,
|};
export type ObjT = {|
  parameters: ParamsT,
  options?: Object,
|};

const getParams = function(obj: ObjT) {
  const { parameters: { data }, options } = obj;
  return {
    method: 'POST',
    url: getBaseURL() + `/api/v1/partner-payment/eprepag/gateway-callback`,
    data: makeFormData(data),
    options,
  };
};

function* Gen(obj: ObjT): Generator<*, Response, *> {
  return yield getHttpRequest()(getParams(obj));
}

export default Gen;
