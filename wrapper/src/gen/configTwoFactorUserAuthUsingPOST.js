// @flow

import { getHttpRequest, getBaseURL } from '../helpers/runtime';
import type { AjaxObject, AjaxResponse } from '../types/AjaxObject';
import type { MemberTfaConfigOut } from '../types/MemberTfaConfigOut';
import type { MemberLoginInputWithTfaTokenAndTfaConfig } from '../types/MemberLoginInputWithTfaTokenAndTfaConfig';
export type Response = AjaxResponse<MemberTfaConfigOut>;
export type ParamsDataT = MemberLoginInputWithTfaTokenAndTfaConfig;
export type ParamsT = {|
  data: ParamsDataT,
|};
export type ObjT = {|
  parameters: ParamsT,
  options?: Object,
|};

const getParams = function(obj: ObjT) {
  const { parameters: { data }, options } = obj;
  return {
    method: 'POST',
    url: getBaseURL() + `/api/v1/member/auth/tfa-config`,
    data,
    options,
  };
};

function* Gen(obj: ObjT): Generator<*, Response, *> {
  return yield getHttpRequest()(getParams(obj));
}

export default Gen;
