// @flow

import { getHttpRequest, getBaseURL } from '../helpers/runtime';
import type { AjaxObject, AjaxResponse } from '../types/AjaxObject';
export type Response = AjaxResponse<any>;
export type ParamsT = {|
  num_rec: number,
|};
export type ObjT = {|
  parameters: ParamsT,
  options?: Object,
|};

const getParams = function(obj: ObjT) {
  const { parameters: { num_rec }, options } = obj;
  return {
    method: 'GET',
    url:
      getBaseURL() +
      `/api/v1/member/history/gamecrs/pay-transaction/main-data/num-rec/${num_rec}`,
    options,
  };
};

function* Gen(obj: ObjT): Generator<*, Response, *> {
  return yield getHttpRequest()(getParams(obj));
}

export default Gen;
