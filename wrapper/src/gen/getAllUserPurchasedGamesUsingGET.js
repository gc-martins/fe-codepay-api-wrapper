// @flow

import { getHttpRequest, getBaseURL } from '../helpers/runtime';
import type { AjaxObject, AjaxResponse } from '../types/AjaxObject';
import type { UserGameCompositeResponse } from '../types/UserGameCompositeResponse';
export type Response = AjaxResponse<UserGameCompositeResponse>;
export type ObjT = {|
  options?: Object,
|};

const getParams = function(obj: ObjT) {
  const { options } = obj;
  return {
    method: 'GET',
    url: getBaseURL() + `/api/v1/game/esprit/user-purchased`,
    options,
  };
};

function* Gen(obj: ObjT): Generator<*, Response, *> {
  return yield getHttpRequest()(getParams(obj));
}

export default Gen;
