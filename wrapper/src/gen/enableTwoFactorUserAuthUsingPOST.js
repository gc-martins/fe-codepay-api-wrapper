// @flow

import { getHttpRequest, getBaseURL } from '../helpers/runtime';
import type { AjaxObject, AjaxResponse } from '../types/AjaxObject';
import type { MemberTfaEnableOut } from '../types/MemberTfaEnableOut';
import type { EnableTwoFactorLoginInput } from '../types/EnableTwoFactorLoginInput';
export type Response = AjaxResponse<MemberTfaEnableOut>;
export type ParamsDataT = EnableTwoFactorLoginInput;
export type ParamsT = {|
  data: ParamsDataT,
|};
export type ObjT = {|
  parameters: ParamsT,
  options?: Object,
|};

const getParams = function(obj: ObjT) {
  const { parameters: { data }, options } = obj;
  return {
    method: 'POST',
    url: getBaseURL() + `/api/v1/member/auth/tfa-enable`,
    data,
    options,
  };
};

function* Gen(obj: ObjT): Generator<*, Response, *> {
  return yield getHttpRequest()(getParams(obj));
}

export default Gen;
