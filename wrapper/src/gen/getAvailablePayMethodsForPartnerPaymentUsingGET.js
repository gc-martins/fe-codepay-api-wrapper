// @flow

import { getHttpRequest, getBaseURL } from '../helpers/runtime';
import type { AjaxObject, AjaxResponse } from '../types/AjaxObject';
import type { PartnerPaymentAvailablePayMethodsOut } from '../types/PartnerPaymentAvailablePayMethodsOut';
export type Response = AjaxResponse<PartnerPaymentAvailablePayMethodsOut>;
export type ObjT = {|
  options?: Object,
|};

const getParams = function(obj: ObjT) {
  const { options } = obj;
  return {
    method: 'GET',
    url: getBaseURL() + `/api/v1/partner-payment/pay-method`,
    options,
  };
};

function* Gen(obj: ObjT): Generator<*, Response, *> {
  return yield getHttpRequest()(getParams(obj));
}

export default Gen;
