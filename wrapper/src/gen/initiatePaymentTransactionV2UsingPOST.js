// @flow

import { getHttpRequest, getBaseURL } from '../helpers/runtime';
import type { AjaxObject, AjaxResponse } from '../types/AjaxObject';
import type { PayTxV2OutObject } from '../types/PayTxV2OutObject';
import type { PayTxV2InObject } from '../types/PayTxV2InObject';
export type Response = AjaxResponse<PayTxV2OutObject>;
export type ParamsDataT = PayTxV2InObject;
export type ParamsT = {|
  data: ParamsDataT,
|};
export type ObjT = {|
  parameters: ParamsT,
  options?: Object,
|};

const getParams = function(obj: ObjT) {
  const { parameters: { data }, options } = obj;
  return {
    method: 'POST',
    url: getBaseURL() + `/api/v1/pay-tx-v2`,
    data,
    options,
  };
};

function* Gen(obj: ObjT): Generator<*, Response, *> {
  return yield getHttpRequest()(getParams(obj));
}

export default Gen;
