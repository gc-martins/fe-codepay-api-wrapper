// @flow

import { getHttpRequest, getBaseURL } from '../helpers/runtime';
import type { AjaxObject, AjaxResponse } from '../types/AjaxObject';
import type { IsEntityForTestApiOut } from '../types/IsEntityForTestApiOut';
export type Response = AjaxResponse<IsEntityForTestApiOut>;
export type ParamsT = {|
  id: string,
|};
export type ObjT = {|
  parameters: ParamsT,
  options?: Object,
|};

const getParams = function(obj: ObjT) {
  const { parameters: { id }, options } = obj;
  return {
    method: 'GET',
    url: getBaseURL() + `/api/v1/is-for-test/user/by-fqid/${id}`,
    options,
  };
};

function* Gen(obj: ObjT): Generator<*, Response, *> {
  return yield getHttpRequest()(getParams(obj));
}

export default Gen;
