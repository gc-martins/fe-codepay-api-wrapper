// @flow

import { getHttpRequest, getBaseURL } from '../helpers/runtime';
import makeFormData from '../helpers/makeFormData';
import type { AjaxObject, AjaxResponse } from '../types/AjaxObject';
export type Response = AjaxResponse<any>;
export type ParamsDataT = {|
  tx_hash?: string,
  address?: string,
  invoice?: string,
  code?: string,
  amount?: string,
  confirmations?: string,
  payout_service_fee?: string,
  payout_miner_fee?: string,
|};
export type ParamsT = {|
  data: ParamsDataT,
|};
export type ObjT = {|
  parameters: ParamsT,
  options?: Object,
|};

const getParams = function(obj: ObjT) {
  const { parameters: { data }, options } = obj;
  return {
    method: 'POST',
    url: getBaseURL() + `/api/v1/gamecredits-bitcoin-callback`,
    data: makeFormData(data),
    options,
  };
};

function* Gen(obj: ObjT): Generator<*, Response, *> {
  return yield getHttpRequest()(getParams(obj));
}

export default Gen;
