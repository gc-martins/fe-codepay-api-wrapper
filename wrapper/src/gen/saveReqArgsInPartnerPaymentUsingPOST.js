// @flow

import { getHttpRequest, getBaseURL } from '../helpers/runtime';
import type { AjaxObject, AjaxResponse } from '../types/AjaxObject';
import type { SaveReqArgsInPartnerPaymentOut } from '../types/SaveReqArgsInPartnerPaymentOut';
import type { SaveReqArgsInPartnerPaymentInp } from '../types/SaveReqArgsInPartnerPaymentInp';
export type Response = AjaxResponse<SaveReqArgsInPartnerPaymentOut>;
export type ParamsDataT = SaveReqArgsInPartnerPaymentInp;
export type ParamsT = {|
  data: ParamsDataT,
|};
export type ObjT = {|
  parameters: ParamsT,
  options?: Object,
|};

const getParams = function(obj: ObjT) {
  const { parameters: { data }, options } = obj;
  return {
    method: 'POST',
    url: getBaseURL() + `/api/v1/partner-payment/req-args-save`,
    data,
    options,
  };
};

function* Gen(obj: ObjT): Generator<*, Response, *> {
  return yield getHttpRequest()(getParams(obj));
}

export default Gen;
