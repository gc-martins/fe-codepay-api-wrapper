// @flow

export type ProcessTransactionRequest = {|
  tx_id: string,
|};
