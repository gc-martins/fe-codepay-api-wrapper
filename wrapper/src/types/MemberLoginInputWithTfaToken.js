// @flow

export type MemberLoginInputWithTfaToken = {|
  client_id: string,
  client_secret: string,
  member_email?: string,
  member_id?: string,
  member_password: string,
  member_phone?: string,
  member_username?: string,
  tfa_token: string,
|};
