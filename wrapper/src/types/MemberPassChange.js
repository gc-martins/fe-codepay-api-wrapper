// @flow

export type MemberPassChange = {|
  new_password: string,
  token: string,
  token_type: string,
|};
