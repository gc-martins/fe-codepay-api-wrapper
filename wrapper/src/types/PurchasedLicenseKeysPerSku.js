// @flow

export type PurchasedLicenseKeysPerSku = {|
  data: Object,
  skuId: string,
|};
