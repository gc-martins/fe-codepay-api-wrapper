// @flow

export type MemberTfaStatusOut = {|
  is_tfa_email?: string,
  result: string,
  tfa_on_pay: string,
  tfa_on_send: string,
|};
