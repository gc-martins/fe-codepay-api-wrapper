// @flow

export type PartnerPaymentPayWithCurrencyGameOut = {|
  tx_id: string,
|};
