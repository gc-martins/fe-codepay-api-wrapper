// @flow

export type PayTxV2InObject = {|
  access_token: string,
  altc_amount: string,
  amount: string,
  currency: string,
  pay_method: string,
  sku_id: string,
  store_id: string,
  tfa_token: string,
|};
