// @flow

export type IsEntityForTestApiOut = {|
  is_for_test: string,
|};
