// @flow

export type MemberTfaEnableOut = {|
  auth_key: string,
  result: string,
|};
