// @flow

export type MerchantForPartnerPayment = {|
  prj_img_url: string,
  prj_name: string,
  prj_return_url: string,
|};
