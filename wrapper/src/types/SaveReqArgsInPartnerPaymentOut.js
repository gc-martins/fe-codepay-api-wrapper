// @flow

export type SaveReqArgsInPartnerPaymentOut = {|
  sig: string,
|};
