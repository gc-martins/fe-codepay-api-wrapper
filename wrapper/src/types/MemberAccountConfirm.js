// @flow

export type MemberAccountConfirm = {|
  auth_token: string,
  email?: string,
|};
