// @flow

export type ProcessTransactionResponse = {|
  order_id: string,
  redirect_url: string,
  tx_id: string,
|};
