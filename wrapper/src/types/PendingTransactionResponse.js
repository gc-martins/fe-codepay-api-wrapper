// @flow

export type PendingTransactionResponse = {|
  amount: any,
  currency: string,
  expired_timestamp: any,
  tx_id: string,
|};
