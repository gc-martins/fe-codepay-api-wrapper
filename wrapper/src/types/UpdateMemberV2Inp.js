// @flow

export type UpdateMemberV2Inp = {|
  data: Object,
  prop: Array<string>,
|};
