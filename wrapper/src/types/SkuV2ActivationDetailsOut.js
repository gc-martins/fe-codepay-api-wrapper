// @flow

export type SkuV2ActivationDetailsOut = {|
  entity_id: string,
|};
