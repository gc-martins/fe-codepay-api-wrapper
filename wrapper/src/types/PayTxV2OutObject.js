// @flow

export type PayTxV2OutObject = {|
  entity_id: string,
  license_key: string,
|};
