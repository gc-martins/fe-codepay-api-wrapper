// @flow

export type SetTfaEmailInp = {|
  tfaEmailStatus: string,
  tfaToken?: string,
|};
