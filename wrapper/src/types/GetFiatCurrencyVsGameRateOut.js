// @flow

export type GetFiatCurrencyVsGameRateOut = {|
  price_fiat: string,
|};
