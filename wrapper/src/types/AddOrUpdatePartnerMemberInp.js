// @flow

export type AddOrUpdatePartnerMemberInp = {|
  add: boolean,
  member_email?: string,
  member_id: string,
  member_phone?: string,
|};
