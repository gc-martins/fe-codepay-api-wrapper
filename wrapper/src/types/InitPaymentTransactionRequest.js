// @flow

export type InitPaymentTransactionRequest = {|
  amount: any,
  created_timestamp: any,
  currency: string,
  notify_url: string,
  order_id: string,
  redirect_url: string,
  signature: string,
|};
