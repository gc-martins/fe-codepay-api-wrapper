// @flow

export type BuyGameResponse = {|
  license_key: string,
  transaction_id?: string,
|};
