// @flow

export type CollectMiningReward = {|
  amount?: string,
  claim?: boolean,
  cpu_mining?: boolean,
  currency: string,
  gpu_mining?: boolean,
|};
