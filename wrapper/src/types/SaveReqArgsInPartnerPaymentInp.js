// @flow

export type SaveReqArgsInPartnerPaymentInp = {|
  from_merchant: string,
  local_pay_method: string,
  to_gateway: string,
|};
