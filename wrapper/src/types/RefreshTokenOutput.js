// @flow

export type RefreshTokenOutput = {|
  access_token: string,
|};
