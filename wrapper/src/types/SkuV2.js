// @flow

export type SkuV2 = {|
  amount: string,
  currency: string,
  entity_id?: string,
  name: string,
  product_id: string,
  stock_limit: number,
  store_id?: string,
|};
