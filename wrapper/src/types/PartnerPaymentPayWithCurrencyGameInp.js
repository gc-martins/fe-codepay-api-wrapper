// @flow

export type PartnerPaymentPayWithCurrencyGameInp = {|
  conv_amount?: string,
  from_merchant: string,
  tfa_token?: string,
|};
