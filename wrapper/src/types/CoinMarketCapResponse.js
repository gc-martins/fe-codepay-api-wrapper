// @flow

export type CoinMarketCapResponse = {|
  available_supply?: string,
  id?: string,
  last_updated?: string,
  market_cap_usd?: string,
  name?: string,
  percent_change_1h?: string,
  percent_change_24h?: string,
  percent_change_7d?: string,
  price_btc?: string,
  price_usd?: string,
  rank?: string,
  symbol?: string,
  total_supply?: string,
|};
