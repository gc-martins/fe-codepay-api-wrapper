// @flow

export type PartnerPaymentAvailablePayMethodsOut = {|
  data: Array<string>,
|};
