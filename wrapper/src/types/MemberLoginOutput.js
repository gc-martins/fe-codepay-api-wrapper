// @flow

export type MemberLoginOutput = {|
  access_token: string,
  expires_in?: number,
  is_tfa: string,
  member_id: string,
  refresh_token: string,
  token_type?: string,
|};
