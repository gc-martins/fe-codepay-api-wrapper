// @flow

import type { PurchasedGameModel } from './PurchasedGameModel';
export type UserGameCompositeResponse = {|
  games?: Array<PurchasedGameModel>,
|};
