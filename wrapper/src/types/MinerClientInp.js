// @flow

export type MinerClientInp = {|
  algorithm?: string,
  app_id: string,
  app_version?: string,
  cpu_brand: string,
  cpu_cores: number,
  cpu_manufacturer: string,
  gpu: Array<Object>,
  os_arch: string,
  os_hostname: string,
  os_platform: string,
|};
