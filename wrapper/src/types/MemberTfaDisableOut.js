// @flow

export type MemberTfaDisableOut = {|
  result: string,
|};
