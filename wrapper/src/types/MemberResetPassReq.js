// @flow

export type MemberResetPassReq = {|
  reason: string,
  user_email: string,
|};
