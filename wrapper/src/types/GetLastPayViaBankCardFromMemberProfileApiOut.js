// @flow

export type GetLastPayViaBankCardFromMemberProfileApiOut = {|
  timestamp: number,
|};
