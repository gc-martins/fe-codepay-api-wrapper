// @flow

export type ChargeTransactionRequest = {|
  agreement_id: string,
  amount: any,
  callback_url: string,
  created_timestamp: any,
  currency: string,
  order_id: string,
  signature: string,
|};
