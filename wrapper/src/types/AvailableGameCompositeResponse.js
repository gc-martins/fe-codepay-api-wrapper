// @flow

import type { GameModel } from './GameModel';
export type AvailableGameCompositeResponse = {|
  games?: Array<GameModel>,
|};
