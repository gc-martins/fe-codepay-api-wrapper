// @flow

export type MinerSaveWorkingData = {|
  algo?: string,
  app_id: string,
  efficiency?: string,
  hash_rate?: string,
  params?: string,
|};
