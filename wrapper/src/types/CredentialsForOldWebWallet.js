// @flow

export type CredentialsForOldWebWallet = {|
  access_token: string,
  old_password: string,
  old_username: string,
|};
