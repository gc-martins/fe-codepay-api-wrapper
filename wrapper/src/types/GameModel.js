// @flow

export type GameModel = {|
  activation_details: string,
  amount: string,
  currency: string,
  description: string,
  img_url: string,
  name: string,
  not_available_in: string,
  platform: string,
  region: string,
  sku_id: string,
  stock_limit: number,
|};
