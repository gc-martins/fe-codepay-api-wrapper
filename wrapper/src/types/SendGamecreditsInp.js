// @flow

export type SendGamecreditsInp = {|
  access_token: string,
  amount: any,
  tfa_token: string,
  to_address: string,
|};
