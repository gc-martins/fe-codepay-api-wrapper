// @flow

export type InitPaymentTransactionResponse = {|
  created_timestamp: any,
  expired_timestamp: any,
  signature: string,
  wallet_url: string,
|};
