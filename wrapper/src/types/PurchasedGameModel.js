// @flow

export type PurchasedGameModel = {|
  activation_details: string,
  amount: string,
  currency: string,
  description: string,
  img_url: string,
  license_key: string,
  name: string,
  not_available_in: string,
  platform: string,
  purchase_timestamp: string,
  region: string,
  sku_id: string,
|};
