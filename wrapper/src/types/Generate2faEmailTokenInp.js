// @flow

export type Generate2faEmailTokenInp = {|
  resend?: boolean,
|};
