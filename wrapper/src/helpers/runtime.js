// @flow

let baseUrl = null;
let httpRequestGen = null;

export const getBaseURL = (): string => {
  if (baseUrl === null) {
    throw Error('you need to set baseUrl');
  }
  return baseUrl;
};
export const getHttpRequest = (): any => {
  if (httpRequestGen === null) {
    throw Error('you need to set httpRequestGen');
  }
  return httpRequestGen;
};
export const setBaseURL = (_baseUrl: string) => (baseUrl = _baseUrl);
export const setHttpRequest = (_httpRequestGen: any) =>
  (httpRequestGen = _httpRequestGen);
