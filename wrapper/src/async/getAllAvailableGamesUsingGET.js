// @flow

import { getHttpRequest, getBaseURL } from '../helpers/runtime';
import type { AjaxObject, AjaxResponse } from '../types/AjaxObject';
import type { AvailableGameCompositeResponse } from '../types/AvailableGameCompositeResponse';
export type Response = AjaxResponse<AvailableGameCompositeResponse>;
export type ObjT = {|
  options?: Object,
|};

const getParams = function(obj: ObjT) {
  const { options } = obj;
  return {
    method: 'GET',
    url: getBaseURL() + `/api/v1/game/esprit/show-all`,
    options,
  };
};

const Async = async (obj: ObjT): Promise<Response> => {
  return await getHttpRequest()(getParams(obj));
};

export default Async;
