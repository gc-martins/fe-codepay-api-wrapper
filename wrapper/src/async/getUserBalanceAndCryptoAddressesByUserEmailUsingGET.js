// @flow

import { getHttpRequest, getBaseURL } from '../helpers/runtime';
import type { AjaxObject, AjaxResponse } from '../types/AjaxObject';
export type Response = AjaxResponse<any>;
export type ParamsT = {|
  user_email: string,
|};
export type ObjT = {|
  parameters: ParamsT,
  options?: Object,
|};

const getParams = function(obj: ObjT) {
  const { parameters: { user_email }, options } = obj;
  return {
    method: 'GET',
    url:
      getBaseURL() +
      `/api/v1/get-user-balance-and-crypto-addresses-by-user-email/${user_email}`,
    options,
  };
};

const Async = async (obj: ObjT): Promise<Response> => {
  return await getHttpRequest()(getParams(obj));
};

export default Async;
