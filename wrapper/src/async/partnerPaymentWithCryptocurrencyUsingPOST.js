// @flow

import { getHttpRequest, getBaseURL } from '../helpers/runtime';
import type { AjaxObject, AjaxResponse } from '../types/AjaxObject';
import type { PartnerPaymentPayWithCurrencyGameOut } from '../types/PartnerPaymentPayWithCurrencyGameOut';
import type { PartnerPaymentPayWithCurrencyGameInp } from '../types/PartnerPaymentPayWithCurrencyGameInp';
export type Response = AjaxResponse<PartnerPaymentPayWithCurrencyGameOut>;
export type ParamsDataT = PartnerPaymentPayWithCurrencyGameInp;
export type ParamsT = {|
  data: ParamsDataT,
|};
export type ObjT = {|
  parameters: ParamsT,
  options?: Object,
|};

const getParams = function(obj: ObjT) {
  const { parameters: { data }, options } = obj;
  return {
    method: 'POST',
    url: getBaseURL() + `/api/v1/partner-payment/pay-with/currency/game`,
    data,
    options,
  };
};

const Async = async (obj: ObjT): Promise<Response> => {
  return await getHttpRequest()(getParams(obj));
};

export default Async;
