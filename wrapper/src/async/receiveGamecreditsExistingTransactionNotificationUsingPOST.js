// @flow

import { getHttpRequest, getBaseURL } from '../helpers/runtime';
import type { AjaxObject, AjaxResponse } from '../types/AjaxObject';
export type Response = AjaxResponse<any>;
export type ParamsDataT = Object;
export type ParamsT = {|
  data: ParamsDataT,
|};
export type ObjT = {|
  parameters: ParamsT,
  options?: Object,
|};

const getParams = function(obj: ObjT) {
  const { parameters: { data }, options } = obj;
  return {
    method: 'POST',
    url: getBaseURL() + `/api/v1/gamecrs-existing-trans-notif`,
    data,
    options,
  };
};

const Async = async (obj: ObjT): Promise<Response> => {
  return await getHttpRequest()(getParams(obj));
};

export default Async;
