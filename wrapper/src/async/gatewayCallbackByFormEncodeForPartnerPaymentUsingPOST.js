// @flow

import { getHttpRequest, getBaseURL } from '../helpers/runtime';
import makeFormData from '../helpers/makeFormData';
import type { AjaxObject, AjaxResponse } from '../types/AjaxObject';
export type Response = AjaxResponse<any>;
export type ParamsDataT = {|
  operation?: string,
  resultCode?: string,
  merNo?: string,
  billNo?: string,
  amount?: string,
  currency?: string,
  dateTime?: string,
  paymentOrderNo?: string,
  remark?: string,
  md5Info?: string,
|};
export type ParamsT = {|
  data: ParamsDataT,
|};
export type ObjT = {|
  parameters: ParamsT,
  options?: Object,
|};

const getParams = function(obj: ObjT) {
  const { parameters: { data }, options } = obj;
  return {
    method: 'POST',
    url:
      getBaseURL() + `/api/v1/partner-payment/gateway-callback-by-form-encode`,
    data: makeFormData(data),
    options,
  };
};

const Async = async (obj: ObjT): Promise<Response> => {
  return await getHttpRequest()(getParams(obj));
};

export default Async;
