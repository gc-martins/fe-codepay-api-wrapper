// @flow

import { getHttpRequest, getBaseURL } from '../helpers/runtime';
import type { AjaxObject, AjaxResponse } from '../types/AjaxObject';
import type { MemberTfaDisableOut } from '../types/MemberTfaDisableOut';
import type { MemberLoginInputWithTfaToken } from '../types/MemberLoginInputWithTfaToken';
export type Response = AjaxResponse<MemberTfaDisableOut>;
export type ParamsDataT = MemberLoginInputWithTfaToken;
export type ParamsT = {|
  data: ParamsDataT,
|};
export type ObjT = {|
  parameters: ParamsT,
  options?: Object,
|};

const getParams = function(obj: ObjT) {
  const { parameters: { data }, options } = obj;
  return {
    method: 'POST',
    url: getBaseURL() + `/api/v1/member/auth/tfa-disable`,
    data,
    options,
  };
};

const Async = async (obj: ObjT): Promise<Response> => {
  return await getHttpRequest()(getParams(obj));
};

export default Async;
