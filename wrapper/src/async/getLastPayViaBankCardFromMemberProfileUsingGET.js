// @flow

import { getHttpRequest, getBaseURL } from '../helpers/runtime';
import type { AjaxObject, AjaxResponse } from '../types/AjaxObject';
import type { GetLastPayViaBankCardFromMemberProfileApiOut } from '../types/GetLastPayViaBankCardFromMemberProfileApiOut';
export type Response = AjaxResponse<
  GetLastPayViaBankCardFromMemberProfileApiOut,
>;
export type ObjT = {|
  options?: Object,
|};

const getParams = function(obj: ObjT) {
  const { options } = obj;
  return {
    method: 'GET',
    url: getBaseURL() + `/api/v1/member/profile/last-pay-via-bank-card`,
    options,
  };
};

const Async = async (obj: ObjT): Promise<Response> => {
  return await getHttpRequest()(getParams(obj));
};

export default Async;
