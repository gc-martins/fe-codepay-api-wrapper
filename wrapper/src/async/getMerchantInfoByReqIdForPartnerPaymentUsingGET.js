// @flow

import { getHttpRequest, getBaseURL } from '../helpers/runtime';
import type { AjaxObject, AjaxResponse } from '../types/AjaxObject';
import type { MerchantForPartnerPayment } from '../types/MerchantForPartnerPayment';
export type Response = AjaxResponse<MerchantForPartnerPayment>;
export type ParamsT = {|
  id: string,
|};
export type ObjT = {|
  parameters: ParamsT,
  options?: Object,
|};

const getParams = function(obj: ObjT) {
  const { parameters: { id }, options } = obj;
  return {
    method: 'GET',
    url: getBaseURL() + `/api/v1/partner-payment/merchant/by-req-id/${id}`,
    options,
  };
};

const Async = async (obj: ObjT): Promise<Response> => {
  return await getHttpRequest()(getParams(obj));
};

export default Async;
