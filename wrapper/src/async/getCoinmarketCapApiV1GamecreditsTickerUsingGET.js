// @flow

import { getHttpRequest, getBaseURL } from '../helpers/runtime';
import type { AjaxObject, AjaxResponse } from '../types/AjaxObject';
import type { CoinMarketCapResponse } from '../types/CoinMarketCapResponse';
export type Response = AjaxResponse<CoinMarketCapResponse>;
export type ObjT = {|
  options?: Object,
|};

const getParams = function(obj: ObjT) {
  const { options } = obj;
  return {
    method: 'GET',
    url:
      getBaseURL() +
      `/api/v1/rates/coinmarketcap-api-v1-gamecredits-ticker-mirror`,
    options,
  };
};

const Async = async (obj: ObjT): Promise<Response> => {
  return await getHttpRequest()(getParams(obj));
};

export default Async;
