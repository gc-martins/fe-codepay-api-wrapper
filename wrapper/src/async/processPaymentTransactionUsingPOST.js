// @flow

import { getHttpRequest, getBaseURL } from '../helpers/runtime';
import type { AjaxObject, AjaxResponse } from '../types/AjaxObject';
import type { ProcessTransactionResponse } from '../types/ProcessTransactionResponse';
import type { ProcessTransactionRequest } from '../types/ProcessTransactionRequest';
export type Response = AjaxResponse<ProcessTransactionResponse>;
export type ParamsDataT = ProcessTransactionRequest;
export type ParamsT = {|
  data: ParamsDataT,
|};
export type ObjT = {|
  parameters: ParamsT,
  options?: Object,
|};

const getParams = function(obj: ObjT) {
  const { parameters: { data }, options } = obj;
  return {
    method: 'POST',
    url: getBaseURL() + `/api/v1/payment/process-transaction`,
    data,
    options,
  };
};

const Async = async (obj: ObjT): Promise<Response> => {
  return await getHttpRequest()(getParams(obj));
};

export default Async;
