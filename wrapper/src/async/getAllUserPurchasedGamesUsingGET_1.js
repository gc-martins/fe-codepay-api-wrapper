// @flow

import { getHttpRequest, getBaseURL } from '../helpers/runtime';
import type { AjaxObject, AjaxResponse } from '../types/AjaxObject';
import type { UserGameCompositeResponse } from '../types/UserGameCompositeResponse';
export type Response = AjaxResponse<UserGameCompositeResponse>;
export type ParamsT = {|
  storeName: string,
|};
export type ObjT = {|
  parameters: ParamsT,
  options?: Object,
|};

const getParams = function(obj: ObjT) {
  const { parameters: { storeName }, options } = obj;
  return {
    method: 'GET',
    url: getBaseURL() + `/api/v1/store/${storeName}/user-purchased`,
    options,
  };
};

const Async = async (obj: ObjT): Promise<Response> => {
  return await getHttpRequest()(getParams(obj));
};

export default Async;
