// @flow

import { getHttpRequest, getBaseURL } from '../helpers/runtime';
import type { AjaxObject, AjaxResponse } from '../types/AjaxObject';
import type { BuyGameResponse } from '../types/BuyGameResponse';
import type { BuyGameRequest } from '../types/BuyGameRequest';
export type Response = AjaxResponse<BuyGameResponse>;
export type ParamsDataT = BuyGameRequest;
export type ParamsT = {|
  storeName: string,
  data: ParamsDataT,
|};
export type ObjT = {|
  parameters: ParamsT,
  options?: Object,
|};

const getParams = function(obj: ObjT) {
  const { parameters: { storeName, data }, options } = obj;
  return {
    method: 'POST',
    url: getBaseURL() + `/api/v1/store/${storeName}/buy-game`,
    data,
    options,
  };
};

const Async = async (obj: ObjT): Promise<Response> => {
  return await getHttpRequest()(getParams(obj));
};

export default Async;
