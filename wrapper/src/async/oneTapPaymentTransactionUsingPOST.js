// @flow

import { getHttpRequest, getBaseURL } from '../helpers/runtime';
import type { AjaxObject, AjaxResponse } from '../types/AjaxObject';
import type { ChargeTransactionRequest } from '../types/ChargeTransactionRequest';
export type Response = AjaxResponse<any>;
export type ParamsDataT = ChargeTransactionRequest;
export type ParamsT = {|
  data: ParamsDataT,
|};
export type ObjT = {|
  parameters: ParamsT,
  options?: Object,
|};

const getParams = function(obj: ObjT) {
  const { parameters: { data }, options } = obj;
  return {
    method: 'POST',
    url: getBaseURL() + `/api/v1/payment/one-tap-transaction`,
    data,
    options,
  };
};

const Async = async (obj: ObjT): Promise<Response> => {
  return await getHttpRequest()(getParams(obj));
};

export default Async;
