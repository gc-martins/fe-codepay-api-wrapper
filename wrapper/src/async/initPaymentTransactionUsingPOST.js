// @flow

import { getHttpRequest, getBaseURL } from '../helpers/runtime';
import type { AjaxObject, AjaxResponse } from '../types/AjaxObject';
import type { InitPaymentTransactionResponse } from '../types/InitPaymentTransactionResponse';
import type { InitPaymentTransactionRequest } from '../types/InitPaymentTransactionRequest';
export type Response = AjaxResponse<InitPaymentTransactionResponse>;
export type ParamsDataT = InitPaymentTransactionRequest;
export type ParamsT = {|
  data: ParamsDataT,
|};
export type ObjT = {|
  parameters: ParamsT,
  options?: Object,
|};

const getParams = function(obj: ObjT) {
  const { parameters: { data }, options } = obj;
  return {
    method: 'POST',
    url: getBaseURL() + `/api/v1/payment/init-transaction`,
    data,
    options,
  };
};

const Async = async (obj: ObjT): Promise<Response> => {
  return await getHttpRequest()(getParams(obj));
};

export default Async;
